package com.udacity.jwdnd.course1.cloudstorage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultPage {
    @FindBy(id = "redirect-home")
    private WebElement homeLink;

    private WebDriver webDriver;


    public ResultPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void clickOnHomePage(){
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", webDriver.findElement(By
                .id(this.homeLink.getAttribute("id"))));
    }
}
