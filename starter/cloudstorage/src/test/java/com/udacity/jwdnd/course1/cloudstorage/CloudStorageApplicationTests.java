package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.services.CredentialsService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class CloudStorageApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private CredentialsService credentialsService;

	@Autowired
	private UserService userService;

	private WebDriver driver;

	private String baseURL;

	private String username;

	private String password;

	private String firstName;

	private String lastName;

	private String noteTitle;

	private String noteDescription;

	private LoginPage loginPage;

	private SignUpPage signUpPage;

	private HomePage homePage;

	private String credUrl;

	private String credUsername;

	private String credPassword;

	@BeforeAll
	static void beforeAll() {
		WebDriverManager.chromedriver().setup();
	}

	@BeforeEach
	public void beforeEach() {
		this.driver = new ChromeDriver();
		this.baseURL = "http://localhost:" + this.port;
		this.username = "testerUser";
		this.password = "testPass";
		this.firstName = "testerFirstName";
		this.lastName = "testerLastName";
		this.noteTitle = "Test 1";
		this.noteDescription = "Testing note description";
		this.credUrl = "http://cloudstorage-test.co.mz";
		this.credUsername = "Test";
		this.credPassword = "P@ssword054";

		this.loginPage = new LoginPage(this.driver);
		this.signUpPage = new SignUpPage(this.driver);
		this.homePage = new HomePage(this.driver);
	}

	@AfterEach
	public void afterEach() {
		if (this.driver != null) {
			driver.quit();
		}
	}

	@Test
	@Order(1)
	public void testUnauthorizedUserAccessRestrictions() throws InterruptedException {
		driver.get(baseURL + "/home");
		Assertions.assertEquals("Login", driver.getTitle());
		loginPage.accessSignUp();
		Assertions.assertEquals("Sign Up", driver.getTitle());
		Thread.sleep(5000);
	}

	@Test
	@Order(2)
	public void testUserSignUpLoginLogoutWorkflow() throws InterruptedException {
		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		Assertions.assertEquals("Home", driver.getTitle());
		homePage.logout();

		Assertions.assertEquals("Login", driver.getTitle());
		driver.get(baseURL + "/home");
		Thread.sleep(3000);
		Assertions.assertEquals("Login", driver.getTitle());
	}

	@Test
	@Order(3)
	public void testNoteAddingAndVerifying() throws InterruptedException {
		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		homePage.addNote(noteTitle, noteDescription);

		Thread.sleep(3000);
		Assertions.assertEquals(true, homePage.noteExists(noteTitle,noteDescription));
	}

	@Test
	@Order(4)
	public void testNoteEditingAndVerifying() throws InterruptedException {
		String noteTitleToEdit = "Teste 2";
		String noteDescriptionEdit = "Testando a nova edicao das notas";

		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		homePage.editNote(noteTitleToEdit, noteDescriptionEdit);
		Thread.sleep(3000);
		Assertions.assertEquals(true, homePage.noteExists(noteTitleToEdit,noteDescriptionEdit), "algum erro ocorreu!");
	}

	@Test
	@Order(5)
	public void testNoteDeletingAndVerifying() throws InterruptedException {
		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		homePage.deleteNote();
		Thread.sleep(3000);
		Assertions.assertEquals(false, homePage.noteExists(noteTitle, noteDescription));

	}

	@Test
	@Order(6)
	public void testCredentialAddingAndVerifying()throws InterruptedException{
		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		homePage.addCredential(credUrl, credUsername, credPassword);
		Thread.sleep(3000);
		Assertions.assertEquals(true, homePage.credentialExists(credUrl, credUsername, credPassword, credentialsService));
		Assertions.assertEquals(true, homePage.credentialPasswordUnecrypted(credPassword));
	}

	@Test
	@Order(7)
	public void testCredentialEditingAndVerifying()throws InterruptedException{
		String credUrlEdit = "http://cloudstorage-new-test.co.mz";
		String credUsernameEdit = "TestNew";
		String credPasswordEdit = "NewP@ssword054";

		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		homePage.editCredential(credUrlEdit, credUsernameEdit, credPasswordEdit);
		Thread.sleep(3000);
		Assertions.assertEquals(true, homePage.credentialExists(credUrlEdit, credUsernameEdit, credPasswordEdit, credentialsService), "algum erro ocorreu!");
	}

	@Test
	@Order(8)
	public void testCredentialDeletingAndVerifying() throws InterruptedException {
		driver.get(baseURL + "/home");
		if(!userService.userExists(username)){
			loginPage.accessSignUp();
			signUpPage.signUpUser(firstName, lastName, username, password);
		}
		loginPage.login(username, password);

		homePage.deleteCredential();
		Assertions.assertEquals(false, homePage.credentialExists(credUrl, credUsername, credPassword, credentialsService));
	}
}
