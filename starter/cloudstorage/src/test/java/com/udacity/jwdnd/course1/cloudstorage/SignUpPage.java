package com.udacity.jwdnd.course1.cloudstorage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignUpPage {

    @FindBy(id = "inputFirstName")
    private WebElement firstNameInput;

    @FindBy(id = "inputLastName")
    private WebElement lastNameInput;

    @FindBy(id = "inputUsername")
    private WebElement usernameInput;

    @FindBy(id = "inputPassword")
    private WebElement passwordInput;

    @FindBy(id = "submit-button")
    private WebElement submitButton;

    @FindBy(id = "success-msg")
    private WebElement successMsg;

    @FindBy(id = "error-msg")
    private WebElement errorMsg;

    @FindBy(id = "login-link")
    private WebElement loginLink;

    private WebDriver webDriver;

    public SignUpPage(WebDriver webDriver){
        PageFactory.initElements(webDriver, this);
        this.webDriver = webDriver;
    }

    private void clickButton(String webId){
        WebElement webElement = webDriver.findElement(By.id(webId));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", webElement);
    }

    public void signUpUser(String firstname, String lastname, String username, String password) {
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.submitButton.getAttribute("id")))));
        js.executeScript("arguments[0].value='" + firstname + "';", webDriver.findElement(By
                .id(this.firstNameInput.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + lastname + "';", webDriver.findElement(By
                .id(this.lastNameInput.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + username + "';", webDriver.findElement(By
                .id(this.usernameInput.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + password + "';", webDriver.findElement(By
                .id(this.passwordInput.getAttribute("id"))));
        clickButton(this.submitButton.getAttribute("id"));
    }

    public void accessLogin(){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.submitButton.getAttribute("id")))));
        clickButton(this.loginLink.getAttribute("id"));
    }


}
