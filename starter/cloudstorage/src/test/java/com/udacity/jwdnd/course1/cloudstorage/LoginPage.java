package com.udacity.jwdnd.course1.cloudstorage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    @FindBy(id = "inputUsername")
    private WebElement usernameInput;

    @FindBy(id = "inputPassword")
    private WebElement passwordInput;

    @FindBy(id = "submit-button")
    private WebElement submitButton;

    @FindBy(id = "signup-link")
    private WebElement signupLink;

    private WebDriver webDriver;

    public LoginPage(WebDriver webDriver){
        PageFactory.initElements(webDriver, this);
        this.webDriver = webDriver;
    }

    private void clickButton(String webId){
        WebElement webElement = webDriver.findElement(By.id(webId));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", webElement);
    }

    public void login(String username, String password){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.submitButton.getAttribute("id")))));
        js.executeScript("arguments[0].value='" + username + "';", webDriver.findElement(By
                .id(this.usernameInput.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + password + "';", webDriver.findElement(By
                .id(this.passwordInput.getAttribute("id"))));
        clickButton(this.submitButton.getAttribute("id"));
    }

    public void accessSignUp(){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.submitButton.getAttribute("id")))));
        clickButton(this.signupLink.getAttribute("id"));
    }
}
