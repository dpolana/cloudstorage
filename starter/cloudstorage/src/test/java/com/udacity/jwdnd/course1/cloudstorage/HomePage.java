package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialsService;
import com.udacity.jwdnd.course1.cloudstorage.services.EncryptionService;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class HomePage {
    @FindBy(css = "#logout-button")
    private WebElement logoutButton;

    @FindBy(id = "nav-notes-tab")
    private WebElement noteNavigator;

    @FindBy(id = "add-note-btn")
    private WebElement noteAddButton;

    @FindBy(id = "note-title")
    private WebElement noteTitle;

    @FindBy(id = "note-description")
    private WebElement noteDescr;

    @FindBy(id = "noteSubmit")
    private WebElement noteSubmit;

    @FindBy(id = "nav-credentials-tab")
    private WebElement credNavigator;

    @FindBy(id = "add-cred-btn")
    private WebElement credAddButton;

    @FindBy(id = "credential-url")
    private WebElement credURL;

    @FindBy(id = "credential-username")
    private WebElement credUsername;

    @FindBy(id = "credential-password")
    private WebElement credPassword;

    @FindBy(id = "credentialSubmit")
    private WebElement credSubmit;

    @FindBy(linkText = "here")
    private WebElement homeLink;

    private WebDriver webDriver;
    private ResultPage resultPage;



    public HomePage(WebDriver webDriver){
        this.webDriver = webDriver;
        this.resultPage = new ResultPage(webDriver);
        PageFactory.initElements(webDriver, this);
    }


    public void logout(){
        clickButton(this.logoutButton.getAttribute("id"));
    }

    private void clickButton(String webId){
        WebElement webElement = webDriver.findElement(By.id(webId));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", webElement);
    }

    public boolean noteExists(String realTitle, String realDescr){
        try {
            String insertedTitle = webDriver.findElement(By.xpath("//*[@id=\"userTable\"]/tbody/tr/th")).getText();
            String insertedDescr = webDriver.findElement(By.xpath("//*[@id=\"userTable\"]/tbody/tr/td[2]")).getText();
            return insertedTitle.equals(realTitle) && insertedDescr.equals(realDescr);
        } catch (NoSuchElementException ex){
            return false;
        }
    }

    public void addNote(String noteTitle, String noteDescription){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;

        //Navigate to the notes tab and click add note button
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.noteNavigator.getAttribute("id")))));
        clickButton(this.noteNavigator.getAttribute("id"));
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.noteAddButton.getAttribute("id")))));
        clickButton(this.noteAddButton.getAttribute("id"));

        // Note title and description filling and submission
        webDriver.switchTo().activeElement();
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                        .id(this.noteTitle.getAttribute("id")))));
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.noteDescr.getAttribute("id")))));
        js.executeScript("arguments[0].value='" + noteTitle + "';", webDriver.findElement(By
                .id(this.noteTitle.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + noteDescription + "';", webDriver.findElement(By
                .id(this.noteDescr.getAttribute("id"))));
        clickButton(this.noteSubmit.getAttribute("id"));

        //return to the home page to navigate back to the notes tab and get filled values
        resultPage.clickOnHomePage();
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.noteNavigator.getAttribute("id")))));
        clickButton(this.noteNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']" +
                        "/div/div[@id='nav-notes']/div[1]/table/tbody/tr/td[1]/button")));
    }

    public void editNote(String noteTitle, String noteDescription){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.noteNavigator.getAttribute("id")))));
        clickButton(this.noteNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']" +
                        "/div/div[@id='nav-notes']/div[1]/table/tbody/tr/td[1]/button")));
        webDriver.switchTo().activeElement();
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.noteTitle.getAttribute("id")))));
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.noteDescr.getAttribute("id")))));
        js.executeScript("arguments[0].value='" + noteTitle + "';", webDriver.findElement(By
                .id(this.noteTitle.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + noteDescription + "';", webDriver.findElement(By
                .id(this.noteDescr.getAttribute("id"))));
        clickButton(this.noteSubmit.getAttribute("id"));

        //return to the home page to navigate back to the notes tab and get filled values
        resultPage.clickOnHomePage();
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.noteNavigator.getAttribute("id")))));
        clickButton(this.noteNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']" +
                        "/div/div[@id='nav-notes']/div[1]/table/tbody/tr/td[1]/button")));
    }

    public void deleteNote(){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.noteNavigator.getAttribute("id")))));
        clickButton(this.noteNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']" +
                        "/div/div[@id='nav-notes']/div[1]/table/tbody/tr/td[1]/a")));
        resultPage.clickOnHomePage();
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.noteNavigator.getAttribute("id")))));
        clickButton(this.noteNavigator.getAttribute("id"));
    }

    public boolean credentialExists(String realUrl, String realUsername, String realPassword,
                                    CredentialsService credService){
        try {
            Credential credential = credService.retrieveCredential(1);
            String insertedUrl = webDriver.findElement(By.xpath("//*[@id=\"credentialTable\"]/tbody/tr/th")).getText();
            String insertedUser = webDriver.findElement(By.xpath("//*[@id=\"credentialTable\"]/tbody/tr/td[2]")).getText();
            String insertedPass = new EncryptionService().decryptValue(webDriver.findElement(By
                    .xpath("//*[@id=\"credentialTable\"]/tbody/tr/td[3]")).getText(), credential.getKey());

            return insertedUrl.equals(realUrl)
                    && insertedUser.equals(realUsername) && insertedPass.equals(realPassword);
        } catch (NoSuchElementException ex){
            return false;
        }
    }

    public boolean credentialPasswordUnecrypted(String realPassword){
        try {
            String insertedPass = webDriver.findElement(By
                    .xpath("//*[@id=\"credentialTable\"]/tbody/tr/td[3]")).getText();
            return !(insertedPass.equals(realPassword)) ;
        } catch (NoSuchElementException ex){
            return false;
        }
    }

    public void addCredential(String url, String username, String password) {
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;

        //Navigate to the credentials tab and click add credemtial button
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.credNavigator.getAttribute("id")))));
        clickButton(this.credNavigator.getAttribute("id"));
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.credAddButton.getAttribute("id")))));
        clickButton(this.credAddButton.getAttribute("id"));

        // credential url, username and password filling and submission
        webDriver.switchTo().activeElement();
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.credURL.getAttribute("id")))));
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.credUsername.getAttribute("id")))));
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.credPassword.getAttribute("id")))));
        js.executeScript("arguments[0].value='" + url + "';", webDriver.findElement(By
                .id(this.credURL.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + username + "';", webDriver.findElement(By
                .id(this.credUsername.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + password + "';", webDriver.findElement(By
                .id(this.credPassword.getAttribute("id"))));
        clickButton(this.credSubmit.getAttribute("id"));

        //return to the home page to navigate back to the notes tab and get filled values
        resultPage.clickOnHomePage();
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.credNavigator.getAttribute("id")))));
        clickButton(this.credNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']/div/div[@id='nav-credentials']" +
                        "/div[1]/table/tbody/tr/td[1]/button")));
    }

    public void editCredential(String url, String username, String password){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        // credential url, username and password filling and submission
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.credNavigator.getAttribute("id")))));
        clickButton(this.credNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']/div/div[@id='nav-credentials']" +
                        "/div[1]/table/tbody/tr/td[1]/button")));
        webDriver.switchTo().activeElement();
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.credURL.getAttribute("id")))));
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.credUsername.getAttribute("id")))));
        waiter.until(ExpectedConditions.visibilityOf(webDriver.findElement(By
                .id(this.credPassword.getAttribute("id")))));
        js.executeScript("arguments[0].value='" + url + "';", webDriver.findElement(By
                .id(this.credURL.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + username + "';", webDriver.findElement(By
                .id(this.credUsername.getAttribute("id"))));
        js.executeScript("arguments[0].value='" + password + "';", webDriver.findElement(By
                .id(this.credPassword.getAttribute("id"))));
        clickButton(this.credSubmit.getAttribute("id"));

        //return to the home page to navigate back to the notes tab and get filled values
        resultPage.clickOnHomePage();
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.credNavigator.getAttribute("id")))));
        clickButton(this.credNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']/div/div[@id='nav-credentials']" +
                        "/div[1]/table/tbody/tr/td[1]/button")));
    }

    public void deleteCredential(){
        WebDriverWait waiter = new WebDriverWait(webDriver, 200);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.id(this.credNavigator.getAttribute("id")))));
        clickButton(this.credNavigator.getAttribute("id"));
        js.executeScript("arguments[0].click();", webDriver.findElement(By
                .xpath("/html/body/div/div[@id='contentDiv']" +
                        "/div/div[@id='nav-credentials']/div[1]/table/tbody/tr/td[1]/a")));
        resultPage.clickOnHomePage();
        waiter.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By
                .id(this.credNavigator.getAttribute("id")))));
        clickButton(this.credNavigator.getAttribute("id"));
    }
}
