package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.File;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface FileMapper {

    @Insert("insert into files (filename, contenttype, filesize, userid, filedata) " +
            "values (#{filename},#{contentType},#{fileSize},#{userId},#{fileData})")
    @Options(useGeneratedKeys = true, keyProperty = "fileId")
    Integer insertFile(File file);

    @Select("select * from files f, users u " +
            "where f.userid = u.userid and u.username=#{username}")
    List<File> listFilesByUsername(String username);

    @Delete("delete files where fileId = #{id}")
    Integer deleteFile(Integer id);

    @Select("select * from files where filename = #{filename}")
    File findByFileName(String filename);
}
