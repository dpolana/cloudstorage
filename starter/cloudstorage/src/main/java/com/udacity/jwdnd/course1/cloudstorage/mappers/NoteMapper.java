package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.Note;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface NoteMapper {
    @Insert("insert into notes (notetitle, notedescription, userid) " +
            "values (#{noteTitle}, #{noteDescription}, #{userId})")
    @Options(useGeneratedKeys = true, keyProperty = "noteId")
    Integer insertNote(Note note);

    @Select("select * from notes n, users u " +
            "where n.userId = u.userId and u.username = #{username}")
    List<Note> getNotesByUsername(String username);

    @Update("update notes " +
            "set notetitle = #{noteTitle}, " +
            "notedescription = #{noteDescription} " +
            "where noteid = #{noteId}")
    Integer editNote(Note note);

    @Delete("delete notes where noteid = #{id}")
    Integer deleteNote(Integer id);
}
