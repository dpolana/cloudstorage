package com.udacity.jwdnd.course1.cloudstorage.mappers;

import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CredentialMapper {

    @Insert("insert into CREDENTIALS (url, username, key, password, userid) " +
            "values (#{url},#{username},#{key},#{password},#{userId})")
   // @Options(useGeneratedKeys = true, keyProperty = "credentialId")
    Integer insertCredential(Credential credential);

    @Select({"select * from CREDENTIALS c, USERS u " +
            "where c.userid = u.userid and u.username = #{username}"})
    List<Credential> getCredentialsByUsername(String username);

    @Update("update CREDENTIALS " +
            "set url = #{url}, " +
            "username = #{username}, " +
            "key = #{key}, " +
            "password = #{password} " +
            "where userid = #{userId}")
    Integer editCredential(Credential credential);

    @Delete("delete CREDENTIALS where userid = #{id}")
    Integer deleteCredential(Integer id);

    @Select("select * from CREDENTIALS where credentialid = #{id}")
    Credential findById(Integer id);
}
