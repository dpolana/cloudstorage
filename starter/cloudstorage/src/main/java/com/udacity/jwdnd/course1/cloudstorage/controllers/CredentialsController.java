package com.udacity.jwdnd.course1.cloudstorage.controllers;

import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialsService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/credential")
public class CredentialsController {
    private CredentialsService credentialsService;
    private UserService userService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public CredentialsController(CredentialsService credentialsService, UserService userService) {
        this.credentialsService = credentialsService;
        this.userService = userService;
    }

    @PostMapping
    public String addOrEditCredential(@ModelAttribute Credential credential, Authentication authentication, RedirectAttributes redirectAttributes){
        String username = authentication.getName();
        logger.info("Username:" +username);
        Integer userId = userService.getIdByUsername(username);
        logger.info("UserID:" +userId);

        try{
            logger.info("Attempting to insert/update a Credential...");
            credential.setUserId(userId);
            credentialsService.insertOrUpdateCredential(credential);
            redirectAttributes.addFlashAttribute("successMessage", "Your credential operation was successfully executed!");
            return "redirect:/home/result";
        } catch (Exception e) {
            logger.error("Error Message: "+e.getMessage()+" Caused by " +e.getCause());
            redirectAttributes.addFlashAttribute("errorMessage", "There was an error with your credential operation. Please try again.");
            return "redirect:/home/result";
        }

    }

    @GetMapping
    public String listCredentials(){
       return "redirect:/home";
    }

    @GetMapping("/delete/{id}")
    public String deleteCredential(@PathVariable Integer id, RedirectAttributes redirectAttributes){
        try {
            logger.debug("Attempting to delete credential with id " + id);
            credentialsService.deleteCredential(id);
            redirectAttributes.addFlashAttribute("successMessage", "Your credential was successfully deleted!");
            return "redirect:/home/result";
        } catch (Exception e) {
            logger.error("Error Message: " + e.getMessage() + " Caused by " + e.getCause());
            redirectAttributes.addFlashAttribute("errorMessage", "Something went wrong! Your credential was not deleted, please try again.");
            return "redirect:/home/result";
        }
    }
}
