package com.udacity.jwdnd.course1.cloudstorage.controllers;

import com.udacity.jwdnd.course1.cloudstorage.models.User;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/signup")
public class SignUpController {
    private UserService userService;

    public SignUpController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getSignUpView(){
        return "signup";
    }

    @PostMapping
    public String signUpUser(Model model, @ModelAttribute User user, RedirectAttributes redirectAttributes){
        String signupError = null;

        if (userService.userExists(user.getUsername())) {
            signupError = "The username "+user.getUsername()+" already exists.";
        }

        if (signupError == null) {
            int rowsAdded = userService.registerUser(user);
            if (rowsAdded < 0) {
                signupError = "There was an error signing you up. Please try again.";
                model.addAttribute("signupError", signupError);
                return "signup";
            }
            redirectAttributes.addFlashAttribute("param.signup", "Success");
            return "redirect:/login";
        } else {
            model.addAttribute("signupError", signupError);
            return "signup";
        }
    }
}
