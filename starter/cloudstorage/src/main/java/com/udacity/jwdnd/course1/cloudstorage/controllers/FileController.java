package com.udacity.jwdnd.course1.cloudstorage.controllers;

import com.udacity.jwdnd.course1.cloudstorage.models.File;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@Controller
@RequestMapping("/file")
public class FileController {

    private UserService userService;
    private FileService fileService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public FileController(UserService userService, FileService fileService) {
        this.userService = userService;
        this.fileService = fileService;
    }

    @PostMapping
    public String uploadFile(@RequestParam("fileUpload") MultipartFile multipartFile, RedirectAttributes redirectAttributes, Authentication authentication) throws IOException {
        logger.info("Attempting to upload a file..");
        if(multipartFile.isEmpty()) {
            logger.error("No file was selected!");
            redirectAttributes.addFlashAttribute("errorMessage", "Please select a file to upload!");
            return "redirect:/home/result";
        }

        try {
            int result;
            String username = authentication.getName();
            File file = new File();
            file.setFilename(multipartFile.getOriginalFilename());
            file.setContentType(multipartFile.getContentType());
            file.setFileSize(String.valueOf(multipartFile.getSize()));
            file.setFileData(multipartFile.getBytes());
            file.setUserId(this.userService.getIdByUsername(username));

            result = fileService.insertFile(file);
            if(result > 0){
                logger.info("Uploaded "+file.getFilename()+" successfully!");
                redirectAttributes.addFlashAttribute("successMessage", "Your file was successfully uploaded");
            } else {
                redirectAttributes.addFlashAttribute("errorMessage", "A file with the name "+file.getFilename()+" already exists! Please change the filename and try again.");
                logger.warn("The file "+file.getFilename()+" exists!");
            }
        } catch (Exception e) {
            logger.error("Error Message: "+e.getMessage()+" Caused by " +e.getCause());
            redirectAttributes.addFlashAttribute("errorMessage", "There was an error uploading your file. Please try again.");
        }
        return "redirect:/home/result";
    }

    @GetMapping
    public String listFiles(){
        return "redirect:/home";
    }

    @GetMapping("/{filename}")
    @ResponseBody
    public ResponseEntity<File> viewOrDownloadFile(@PathVariable String filename){
        logger.info("Downloading the file "+ filename+"...");
        File file = fileService.loadFile(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/delete/{id}")
    public String removeFile(@PathVariable Integer id, RedirectAttributes redirectAttributes) {
        try {
            logger.info("Attempting to delete file with id " + id);
            fileService.deleteFile(id);
            redirectAttributes.addFlashAttribute("successMessage", "Your file was successfully deleted!");
            return "redirect:/home/result";
        } catch (Exception e) {
            logger.error("Error Message: " + e.getMessage() + " Caused by " + e.getCause());
            redirectAttributes.addFlashAttribute("errorMessage", "Something went wrong! Your file was not deleted, please try again.");
            return "redirect:/home/result";
        }
    }
}
