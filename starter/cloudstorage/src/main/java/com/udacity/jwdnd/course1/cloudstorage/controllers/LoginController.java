package com.udacity.jwdnd.course1.cloudstorage.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String loginView(Model model) {
        String attribute = (String) model.asMap().get("param.signup");
        if(attribute != null) {
            model.addAttribute("signupSuccess", true);
        }
        return "login";
    }

    @GetMapping("/logout")
    public String logoutView(){ return "redirect:/login?logout"; }
}
