package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mappers.FileMapper;
import com.udacity.jwdnd.course1.cloudstorage.models.File;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    private FileMapper fileMapper;

    public FileService(FileMapper fileMapper) {
        this.fileMapper = fileMapper;
    }

    public boolean fileExists(File file){
        String filename = file.getFilename();
        return loadFile(filename)==null;
    }

    public Integer insertFile(File file){
        if(!fileExists(file)){
            return 0;
        } else {
            return fileMapper.insertFile(new File(null, file.getFilename(), file.getContentType(),
                    file.getFileSize(), file.getUserId(), file.getFileData()));
        }
    }

    public List<File> listFiles(String username){
        return this.fileMapper.listFilesByUsername(username);
    }

    public File loadFile(String filename){
        return fileMapper.findByFileName(filename);
    }

    public Integer deleteFile(Integer id){
        return this.fileMapper.deleteFile(id);
    }
}
