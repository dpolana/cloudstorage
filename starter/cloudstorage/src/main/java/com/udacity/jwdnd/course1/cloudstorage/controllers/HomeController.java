package com.udacity.jwdnd.course1.cloudstorage.controllers;

import com.udacity.jwdnd.course1.cloudstorage.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/home")
public class HomeController {

    private EncryptionService encryptionService;
    private CredentialsService credentialsService;
    private NoteService noteService;
    private FileService fileService;

    public HomeController(EncryptionService encryptionService, CredentialsService credentialsService, NoteService noteService, FileService fileService) {
        this.encryptionService = encryptionService;
        this.credentialsService = credentialsService;
        this.noteService = noteService;
        this.fileService = fileService;
    }

    @GetMapping
    public String homeView(Authentication authentication, Model model) throws Exception{
        String username = authentication.getName();
        model.addAttribute("notes", this.noteService.listNotes(username));
        model.addAttribute("credentials", this.credentialsService.listCredentialsByUser(username));
        model.addAttribute("files", this.fileService.listFiles(username));
        model.addAttribute("encryptionService", this.encryptionService);
        return "home";
    }

    @GetMapping("/result")
    public String resultView(){
        return "result";
    }
}
