package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mappers.NoteMapper;
import com.udacity.jwdnd.course1.cloudstorage.models.Note;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {
    private final NoteMapper noteMapper;
    private final UserService userService;

    public NoteService(NoteMapper noteMapper, UserService userService) {
        this.noteMapper = noteMapper;
        this.userService = userService;
    }

    public Integer createNote(Note note){
        return noteMapper.insertNote(new Note(null, note.getNoteTitle(), note.getNoteDescription(), note.getUserId()));
    }

    public Integer alterNote(Note note){
        return noteMapper.editNote(note);
    }

    public List<Note> listNotes(String username){
        return userService.userExists(username)? noteMapper.getNotesByUsername(username) : null;
    }

    public Integer removeNote(Integer id){
        return noteMapper.deleteNote(id);
    }
}
