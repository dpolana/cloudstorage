package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mappers.CredentialMapper;
import com.udacity.jwdnd.course1.cloudstorage.models.Credential;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;

@Service
public class CredentialsService {
    private CredentialMapper credentialMapper;
    private EncryptionService encryptionService;


    public CredentialsService(CredentialMapper credentialMapper, EncryptionService encryptionService) {
        this.credentialMapper = credentialMapper;
        this.encryptionService = encryptionService;
    }

    public Integer insertOrUpdateCredential(Credential credential) {
        SecureRandom random = new SecureRandom();
        String rawPassword = credential.getPassword();
        byte[] key = new byte[16];
        random.nextBytes(key);
        String encodedKey = Base64.getEncoder().encodeToString(key);
        String encryptedPassword = encryptionService.encryptValue(rawPassword, encodedKey);

        if(credential.getCredentialId() > 0){
            credential.setKey(credential.getKey());
            credential.setPassword(encryptionService.encryptValue(rawPassword, credential.getKey()));
            return credentialMapper.editCredential(credential);
        } else {
            return credentialMapper.insertCredential(new Credential(null, credential.getUrl(),
                    credential.getUsername(), encodedKey, encryptedPassword, credential.getUserId()));
        }
    }

    public List<Credential> listCredentialsByUser(String username){
        return credentialMapper.getCredentialsByUsername(username);
    }

    public Integer deleteCredential(Integer id){
        return credentialMapper.deleteCredential(id);
    }

    public Credential retrieveCredential(Integer id){
        return credentialMapper.findById(id);
    }
}
