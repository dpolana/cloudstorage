package com.udacity.jwdnd.course1.cloudstorage.controllers;

import com.udacity.jwdnd.course1.cloudstorage.models.Note;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/note")
public class NoteController {

    private NoteService noteService;
    private UserService userService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public NoteController(NoteService noteService, UserService userService) {
        this.noteService = noteService;
        this.userService = userService;
    }

    @PostMapping
    public String postOrUpdateNote(@ModelAttribute Note note, Authentication authentication, RedirectAttributes redirectAttributes){
        String username = authentication.getName();
        Integer userId = userService.getIdByUsername(username);
        note.setUserId(userId);

        if(note.getNoteId() > 0) {
            logger.info("Entered noteUpdate condition..");
            try {
                noteService.alterNote(note);
                redirectAttributes.addFlashAttribute("successMessage", "Your note was successfully updated!");
                return "redirect:/home/result";
            } catch (Exception e) {
                logger.error("Error Message: "+e.getMessage()+" Caused by " +e.getCause());
                redirectAttributes.addFlashAttribute("errorMessage", "Your note was not updated! Please try again.");
                return "redirect:/home/result";
            }
        } else {
            logger.info("Entered noteCreate condition");
            try {
                noteService.createNote(note);
                redirectAttributes.addFlashAttribute("successMessage", "Your note was successfully inserted!");
                return "redirect:/home/result";
            } catch (Exception e){
                logger.error("Error Message: "+e.getMessage()+" Caused by " +e.getCause());
                redirectAttributes.addFlashAttribute("errorMessage", "There was an error inserting your note. Please try again.");
                return "redirect:/home/result";
            }

        }
    }

    @GetMapping
    public String getNotes(){
        return "redirect:/home";
    }

    @GetMapping("/delete/{id}")
    public String deleteNote(@PathVariable Integer id, RedirectAttributes redirectAttributes){
        try {
            logger.debug("Attempting to delete note with id "+id);
            noteService.removeNote(id);
            redirectAttributes.addFlashAttribute("successMessage", "Your note was successfully deleted!");
            return "redirect:/home/result";
        } catch (Exception e) {
            logger.error("Error Message: "+e.getMessage()+" Caused by " +e.getCause());
            redirectAttributes.addFlashAttribute("errorMessage", "Something went wrong! Your note was not deleted, please try again.");
            return "redirect:/home/result";
        }
    }

}
